const DialogFlow = require('dialogflow');

class DialogProcessor {
  constructor(config) {
    const { projectId } = config;
    this.projectId = projectId;
  }

  run = async (sessionId, text) => {
    const sessionClient = new DialogFlow.SessionsClient();
    const sessionPath = sessionClient.sessionPath(this.projectId, sessionId);

    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          text,
          languageCode: 'id-ID'
        }
      }
    };

    const responses = await sessionClient.detectIntent(request);
    console.log(JSON.stringify(responses));

    const result = responses[0].queryResult;

    return result;
  }
}

module.exports = DialogProcessor;