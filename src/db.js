const Sequelize = require('sequelize');
let sequelize;

if (process.env.DATABASE_URL) {
  sequelize = new Sequelize(process.env.DATABASE_URL);
} else {
  sequelize = new Sequelize('weshare', 'weshare', 'weshare', {
    host: 'localhost',
    dialect: 'postgres'
  });
}

sequelize.authenticate()
  .then(() => console.log('Connection has been established'))
  .catch(error => console.error('Unable to connect to database', error));

module.exports = sequelize;
