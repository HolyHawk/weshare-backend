const dotenv = require('dotenv');
dotenv.config();

const morgan = require('morgan');
const express = require('express');
const line = require('@line/bot-sdk');
const bodyParser = require('body-parser');
const DialogProcessor = require('./util/processor');

const {Line} = require('./controller');

const router = require('./router');

const config = {
  channelAccessToken: process.env.CHANNEL_ACCESS_TOKEN,
  channelSecret: process.env.CHANNEL_SECRET,
};

const app = express();
const port = process.env.PORT || 3132;

const processor = new DialogProcessor({
  projectId: process.env.PROJECT_ID || '',
});

const lineController = new Line(config, processor);

app.get('/webhook', (req, res) => res.status(200).send('Working, please use POST'));
app.post('/webhook', line.middleware(config), lineController.webhook);

app.use(morgan('dev'));
app.use(bodyParser());
app.get('/', (req, res) => res.status(200).send('Hello World'));
app.use(router);

app.listen(port, () => console.log(`Listening on port ${port}`));
