const {User} = require('../controller');
const {Router} = require('express');

const router = Router();

router.get('/wallet/:id');
router.get('/user/:id', User.profile);
router.post('/withdraw', User.withdraw);

module.exports = router;