const {User, Organization, Transaction, Wallet} = require('../model');

exports.profile = async (req, res) => {
  const id = req.params.id;
  const user = await User.findOne({
    where: {id},
    include: [{
      model: Organization,
      include: [{
        model: Wallet,
        include: [Transaction]
      }]
    }]
  });
  console.log(user);
  res.status(200).send(user)
};

exports.withdraw = async (req, res) => {
  const from = req.body.from;
  const to = req.body.to;
  const amount = req.body.amount;
  const transaction = await Transaction.create({
    walletId: parseInt(from),
    userId: parseInt(to),
    amount
  });
  const wallet = await Wallet.findOne({
    where: {id: parseInt(from)}
  });
  await wallet.update({ balance: wallet.balance - amount });
  res.status(200).send(transaction);
};
