const line = require('@line/bot-sdk');
const {Transaction, Wallet} = require('../model');

class Line {
  constructor(config, processor) {
    this.client = new line.Client(config);
    this.processor = processor;
  }

  handleEvent = event => {
    if (event.type === 'join') {
      console.log(event.source.groupId);
      return Promise.resolve(null);
    }

    if (event.type === 'message' && event.message.type === 'text') {
      return this.handleTextMessage(event);
    }

    if (event.type === 'message' && event.message.type === 'sticker') {
      return this.client.replyMessage(event.replyToken, {
        type: 'sticker',
        stickerId: event.message.stickerId
      });
    }

    return Promise.resolve(null);
  };

  handleTextMessage = async ({ message, replyToken, source }) => {
    const response = await this.processor.run(source.userId, message.text);
    if (response.intent.displayName === 'TransferAmount') {
      const transaction = await Transaction.create({
        walletId: 1,
        userId: 1,
        amount: response.parameters.fields.number.numberValue
      });
      const wallet = await Wallet.findOne({
        where: {id: 1}
      });
      wallet.update({
        balance: wallet.balance - transaction.amount
      });
    }
    if (response.intent.displayName === 'Balance') {
      const wallet = await Wallet.findOne({
        where: {id: 1}
      });
      return this.client.replyMessage(replyToken, {
        type: 'text',
        text: response.fulfillmentText.replace('[balance]', wallet.balance),
      });
    }
    return this.client.replyMessage(replyToken, {
      type: 'text',
      text: response.fulfillmentText,
    });
  };

  webhook = async (req, res) => {
    const result = await Promise.all(req.body.events.map(this.handleEvent));
    res.json(result);
  }
}

module.exports = Line;