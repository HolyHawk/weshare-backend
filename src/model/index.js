const Sequelize = require('sequelize');
const sequelize = require('../db');

const Organization = require('./Organization');
const Transaction = require('./Transaction');
const Wallet = require('./Wallet');
const User = require('./User');

const syncDatabase = async () => {
  Organization.belongsToMany(User, { through: 'user_organization' });
  User.belongsToMany(Organization, { through: 'user_organization' });
  Organization.hasOne(Wallet);
  Wallet.hasMany(Transaction);

  await sequelize.sync({ force: true });

  const organization = await Organization.create({
    id: 1,
    groupId: 'C20bdac9409a25f080f9fb921e4d35530',
    groupName: 'WeShare',
    users: [
      {
        id: 1,
        lineId: '',
        name: 'Tanor Abraham',
        profilePicture: 'http://help.wojilu.com/metronic/theme/assets/admin/pages/media/profile/profile_user.jpg',
      }
    ],
  }, {
    include: [ User ]
  });

  await Wallet.create({
    id: 1,
    balance: 5000000,
    organizationId: 1
  });
};

syncDatabase().then();

module.exports = {
  Organization,
  Transaction,
  Wallet,
  User
};
