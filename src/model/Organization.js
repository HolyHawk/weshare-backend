const Sequelize = require('sequelize');
const sequelize = require('../db');
const Model = Sequelize.Model;

class Organization extends Model {}
Organization.init({
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  groupId: {
    type: Sequelize.STRING,
    allowNull: false
  },
  groupName: {
    type: Sequelize.STRING,
    allowNull: false
  },
}, {
  sequelize,
  modelName: 'organization'
});

module.exports = Organization;