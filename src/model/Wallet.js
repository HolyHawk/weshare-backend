const Sequelize = require('sequelize');
const sequelize = require('../db');
const Model = Sequelize.Model;

class Wallet extends Model {}
Wallet.init({
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  balance: {
    type: Sequelize.INTEGER,
    allowNull: false
  }
}, {
  sequelize,
  modelName: 'wallet'
});

module.exports = Wallet;

