const Sequelize = require('sequelize');
const sequelize = require('../db');
const Model = Sequelize.Model;

class Transaction extends Model {}
Transaction.init({
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  amount: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  userId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
}, {
  sequelize,
  modelName: 'transaction'
});

module.exports = Transaction;
